# Pull code
cd /assignment
git checkout master
git pull origin master

# Build and deploy
npm install
npm run build
pm2 restart 0