#!/bin/bash

DEPLOY_SERVER="43.204.3.249"

echo "Deploying to ${DEPLOY_SERVER}"
sudo su
ssh root@${DEPLOY_SERVER} 'bash' < ./deploy/server.sh